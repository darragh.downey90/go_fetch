FROM golang:1.15-buster as builder

RUN apt update && \
 apt -y install libzmq3-dev

# enable go modules
ENV GO111MODULE=on

WORKDIR /app/server

# bug preventing build using go modules
COPY go.* .
COPY *.go .

# RUN go mod download
ENV PATH="$PATH:$(go env GOPATH)/bin"
RUN CGO_ENABLED=1 GOOS=linux go build -o fetch


FROM ubuntu:20.04
WORKDIR /app/server
RUN apt update && apt -y install libzmq3-dev
COPY --from=builder /app/server .

# go build takes the name of the outer dir by default
# can specify name by go build -o pinger
CMD ["./fetch"]
