package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"path"
	"time"

	"github.com/jlaffaye/ftp"
)

var (
	filePath    = os.Getenv("FILEPATH")
	sftpHost    = os.Getenv("SFTPHOST")
	sftpAltHost = os.Getenv("SFTPALTERNATE")
	sftpPort    = os.Getenv("SFTPPORT")
	sftpUser    = os.Getenv("SFTPUSER")
	sftpPass    = os.Getenv("SFTPPASSWD")
)

// ServerConn is a wrapper around github.com/jlaffaye/ftp.ServerConn
type ServerConn struct {
	Conn *ftp.ServerConn
}

// Connect opens connection to FTP server, returns connection object or error
func Connect() (*ServerConn, error) {
	c, err := ftp.Dial(sftpHost+":"+sftpPort, ftp.DialWithTimeout(5*time.Second))

	if err != nil {
		log.Printf("SFTP server details not accepted: %v", err)
		c, err = ftp.Dial(sftpAltHost+":"+sftpPort, ftp.DialWithTimeout(5*time.Second))
		if err != nil {
			log.Printf("SFTP Alternate server rejected request: %v", err)
		}
		return &ServerConn{}, err
	}

	if err = c.Login(sftpUser, sftpPass); err != nil {
		log.Printf("SFTP credentials not accepted: %v", err)
		return &ServerConn{}, err
	}

	return &ServerConn{Conn: c}, nil
}

// Retrieve grabs the file, if it exists, from the nominated location
// returns an array of bytes
func Retrieve(c *ServerConn, filename string) ([]byte, error) {
	r, err := c.Conn.Retr(path.Join(filePath, filename))
	if err != nil {
		log.Printf("Issue connecting to file hosted on FTP server: %v", err)
		return nil, err
	}

	buf, err := ioutil.ReadAll(r)
	if err != nil {
		log.Printf("Unable to read file: %v", err)
		return nil, err
	}

	return buf, nil
}

// Push a given file to a given destination for an SFTP connection
func Push(c *ServerConn, filePath, destination string) (bool, error) {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Printf("Failed to read %v : %v", filePath, err)
		return false, err
	}
	r := bytes.NewReader(data)
	err = c.Conn.Stor(destination, r)
	if err != nil {
		log.Printf("Failed to push %v : %v", filePath, err)
		return false, err
	}
	return true, nil
}

// Exists performs a list operation and then compares
func Exists(c *ServerConn, filenames ...string) (map[string]bool, error) {
	entries, err := c.Conn.List(filePath)
	if err != nil {
		log.Printf("Unable to list entries: %v", err)
	}
	exists := make(map[string]bool)

	// change this
	for _, entry := range entries {
		for _, fname := range filenames {
			if fname == entry.Name {
				exists[fname] = true
			} else {
				exists[fname] = false
			}
		}
	}
	// change this
	return exists, err
}
