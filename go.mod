module gitlab.com/darragh.downey90/go_fetch

go 1.15

require (
	github.com/jlaffaye/ftp v0.0.0-20201021201046-0de5c29d4555
	github.com/pebbe/zmq4 v1.2.1
)
