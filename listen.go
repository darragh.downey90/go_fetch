package main

import (
	"log"
	"os"

	zmq "github.com/pebbe/zmq4"
)

var (
	connStr = os.Getenv("WORKURI")
)

// Listen a driver for pushing/pulling files to/from
// a given SFTP server
func Listen() {
	ch := make(chan string)
	res := make(chan bool)
	resMsg := make(chan string)

	rep, err := zmq.NewSocket(zmq.REP)
	if err != nil {
		log.Printf("Failed to register a REP socket! %v\n", err)
	}
	defer rep.Close()
	rep.Bind(connStr)

	go listening(ch, res, resMsg, rep)

	for {
		msg, ok := <-ch
		if ok == false {
			log.Printf("Breaking listening loop due to issue with receiver\n")
			break
		}
		log.Printf("Received %v %v\n", msg, ok)
		// open connection to SFTP server
		conn, err := Connect()
		if err != nil {
			log.Printf("Failed to create connection to SFTP server\n")
		}

		controller(conn, msg, res, resMsg)
	}
}

func listening(ch chan string, res chan bool, resMsg chan string, rep *zmq.Socket) {
	for {
		// listen for requests
		msg, err := rep.Recv(0)
		if err != nil {
			log.Printf("Issue with message %v\n", err)
			// notify client that message was a fail
			rep.Send("Failed", 0)
		} else {
			log.Printf("Message received: %v\n", msg)
			ch <- msg
			// Process the message sent to ch
			// wait for success true/false
			// receive message from channel *is blocking*
			success := <-res
			successMsg := <-resMsg
			if success {
				// file operation was a success!
				rep.Send(successMsg, 0)
			} else {
				// something went wrong, check the logs
				rep.Send(successMsg, 0)
			}
		}
	}
}
