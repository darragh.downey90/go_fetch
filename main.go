package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	Listen()
}

func controller(conn *ServerConn, message string, res chan bool, resMsg chan string) {
	// message should look like
	// [PUSH|PULL] [FILE] [TO|FROM] [DESTINATION]
	msg := strings.Split(message, " ")
	switch msg[0] {
	case "PULL":
		data, err := Retrieve(conn, msg[1])
		if err != nil {
			res <- false
			resMsg <- fmt.Sprintf("Failed to pull file %v to %v", msg[1], msg[3])
		} else {
			// write to destination
			ioutil.WriteFile(msg[3], data, 0666)
			// send response to channel
			res <- true
			resMsg <- fmt.Sprintf("Successfully pulled %v to %v", msg[1], msg[3])
		}
	case "PUSH":
		success, err := Push(conn, msg[1], msg[3])
		if err != nil {
			// send response to channel
			res <- success
			resMsg <- fmt.Sprintf("Failed to push %v to %v", msg[1], msg[3])
		} else {
			res <- success
			resMsg <- fmt.Sprintf("Successfully pushed %v to %v", msg[1], msg[3])
		}
	default:
		log.Printf("Message unclear %v", message)
		res <- false
		resMsg <- "Message unclear"
	}

	// close the connection
	conn.Conn.Quit()
}
